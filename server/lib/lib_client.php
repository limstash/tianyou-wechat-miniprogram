<?php

    /**
     * 执行库 : 客户端操作 (/lib/lib_client.php)
     */

    class client {
        public $code;
        public $data;
        private $user;

        public function __construct($needAuth, $token="") {
            if ($needAuth && empty($token)) {
                $this->code = 301;
                $this->print();
                exit;
            }

            if ($needAuth && !$this->user = getUserInfoByKeywords("token", $token)) {
                $this->code = 302;
                $this->print();
                exit;
            }
        }

        public function checkPermission($name) {
            if ($this->user["permission"][$name] != 1) {
                $this->code = 403;
                $this->print();
                exit;
            }
        }

        public function login($code) {
            $grant = "authorization_code";

            $params = "appid=".appid."&secret=".appsecret."&js_code=".$code."&grant_type=".$grant;

            $res = request("https://api.weixin.qq.com/sns/jscode2session?".$params, "GET", null);
        
            $resData = json_decode($res, true);

            $openid = $resData["openid"];
            $token = $resData["session_key"];

            if (empty($openid)) {
                $this->code = 303;
            } else {
                if (!$uid = getUserInfoByKeywords("openID", $resData["openid"])["id"]) {
                    db::query("INSERT INTO `app_users` (`openid`) VALUES ('$openid')");
                }

                $this->user = getUserInfoByKeywords("openID", $resData["openid"]);
                $uid = $this->user["id"];

                db::query("UPDATE `app_users` set `token` = '$token' where `id` = '$uid'");

                $this->code = 200;
                $this->data = array(
                    "uid" => $uid,
                    "token" => $token,
                    "status" => $this->user["status"]
                );
            }
        }

        public function verify($ecjtuID, $realname) {
            $uid = $this->user["id"];

            if (!$verify = db::selectFirst("select * from `app_verify_token` where `ecjtuID` = '$ecjtuID' and `realname` = '$realname'")) {
                $this->code = 304;
                return "";
            }
            
            $token = frame::randString(64);
            
            db::query("update `app_users` SET `status` = '1' where `id` = '$uid'");
            db::query("update `app_verify_token` SET `uid` = '$uid' where `ecjtuID` = '$ecjtuID'");
            db::query("update `app_verify_token` SET `token` = '$token' where `ecjtuID` = '$ecjtuID'");

            $this->code = 200;
            return $token;
        }

        public function sendVerifyMail($ecjtuID, $nickname, $token) {
            $smtpemailto = $ecjtuID."@ecjtu.edu.cn";
            $mailtitle = "天佑学院预约小程序账户绑定验证";
            $mailcontent = "请点击 <a href='https://tianyou-wechat.limstash.com/verify/".$token."'>https://tianyou-wechat.limstash.com/verify/".$token."</a> 验证";
            sendMail($smtpemailto, $mailtitle, $mailcontent);
        }

        public function getUserCount() {
            $this->code = 200;
            $this->data = array(
                "orderCount" => $this->getFutureOrderListCount(),
                "bookCount" => $this->getNotReturnBookCount()
            );
        }

        public function getVerifyInfo() {
            $uid = $this->user["id"];

            $verify = db::selectFirst("select * from `app_verify_token` where `uid` = '$uid'");

            $this->code = 200;
            $this->data = array(
                "status" => $this->user["status"],
                "ecjtuID" => $verify["ecjtuID"],
                "realname" => $verify["realname"]
            );
        }

        public function resendVerifyEmail($nickname) {
            $uid = $this->user["id"];

            $verify = db::selectFirst("select * from `app_verify_token` where `uid` = '$uid'");

            $this->sendVerifyMail($verify["ecjtuID"], $nickname, $verify["token"]);
            $this->code = 200;
        }

        public function getTimeBlock() {
            $block = 1;

            switch ($minutes = date("i")) {
                case $minutes >= 45:
                    $block = 3;
                    break;
                case $minutes >= 30:
                    $block = 2;
                    break;
                case $minutes >= 15:
                    $block = 1;
                    break;
            }

            $block += 4 * (date("H") - 8); 
        }

        public function getHistoryOrderList() {
            $date = date("Y-m-d");
            $timeblock = $this->getTimeBlock();

            $this->code = 200;
            $this->data = array(
                "data" => db::selectAll("SELECT * FROM `app_order` WHERE `date` < '$date' or (`date` = '$date' and `endBlock` <= '$timeblock')")
            );
        }

        private function getFutureOrderListCount() {
            $uid = $this->user["id"];
            $date = date("Y-m-d");
            $timeblock = $this->getTimeBlock();
            
            return db::num_rows("SELECT * FROM `app_order` WHERE `uid` = '$uid' and (`date` > '$date' or (`date` = '$date' and `endBlock` >= '$timeblock'))");            
        }

        private function getNotReturnBookCount() {
            $uid = $this->user["id"];
            $date = date("Y-m-d");
            
            return db::num_rows("SELECT * FROM `app_borrow` WHERE `uid` = '$uid' and `endDate` >= '$date' and `status` = '1'");   
        }

        public function getNotReturnBook() {
            $arr = array();
            $uid = $this->user["id"];
            $date = date("Y-m-d");

            $booklist = db::selectAll("SELECT * FROM `app_borrow` WHERE `uid` = '$uid' and `endDate` >= '$date' and `status` = '1' order by `endDate` asc");

            foreach ($booklist as $book) {
                $bookInfo = db::selectFirst("SELECT * FROM `app_booklist` where `id` = '".$book["bookid"]."'");

                $bookInfo = $this->getBookExtendInfo($bookInfo);

                $book["name"] = $bookInfo["name"];
                $book["author"] = $bookInfo["author"];
                $book["publish"] = $bookInfo["publish"];
                $book["isbn"] = $bookInfo["isbn"];

                array_push($arr, $book);
            }

            $this->code = 200;
            $this->data = array(
                "data" => $arr
            );
        }

        public function getFutureOrderList() {
            $uid = $this->user["id"];
            $date = date("Y-m-d");
            $timeblock = $this->getTimeBlock();
            
            $orderlist = db::selectAll("SELECT * FROM `app_order` WHERE `uid` = '$uid' and (`date` > '$date' or (`date` = '$date' and `endBlock` >= '$timeblock'))order by `date`, `startblock` asc");

            $data = array();

            foreach ($orderlist as $order) {
                $rid = $order["rid"];

                $roomInfo = db::selectFirst("SELECT * FROM `app_rooms` where `id` = '$rid'");

                $order = array_merge($order, array(
                    "roomInfo" => $roomInfo
                ));

                array_push($data, $order);
            }

            $this->code = 200;
            $this->data = array(
                "data" => $data
            );
        }

        private function isConflict($order, $startblock, $endblock) {
            if (max($order["startBlock"], $startblock) <= min($order["endBlock"], $endblock)) {
                return true;
            }
            return false;
        }

        public function search($type, $startblock, $endblock, $date) {
            if ($startblock >= $endblock) {
                $this->code = 305;
                return;
            }

            $query_rooms = db::selectAll("SELECT * FROM `app_rooms` where `cid` = '$type'");
            
            $available = array();
            $busy = array();

            foreach ($query_rooms as $room) {
                $rid = $room["id"];
                $query_room_order = db::selectAll("SELECT * FROM `app_order` where `rid` = '$rid' and `date` = '$date'");
                
                $busy_flag = false;

                $room = array_merge($room, array(
                    "people" => count($query_room_order)
                ));

                foreach ($query_room_order as $order) {
                    if ($this->isConflict($order, $startblock, $endblock)) {
                        $busy_flag = true;
                        array_push($busy, $room);
                        break;
                    }
                }

                if (!$busy_flag) {
                    array_push($available, $room);
                }
            }

            $this->code = 200;
            $this->data = array(
                "available" => $available,
                "busy" => $busy
            );
        }

        public function tempOrderRequest() {
            $uid = $this->user["id"];
            $tempOrder = $this->user["tempOrder"];
            $date = date("Y-m-d");

            if ($tempOrder == -1) {
                db::query("INSERT INTO `app_order_temp` (`uid`, `date`, `status`) VALUES ('$uid', '$date', '0')");
            }

            $this->code = 200;
        }

        public function updateTempOrderRequest($id) {
            db::query("UPDATE `app_order_temp` SET `status` = '1' where `id` = '$id'");
            $this->code = 200;
        }

        public function getTempOrderList() {
            $date = date("Y-m-d");
            $res = db::selectAll("SELECT * FROM `app_order_temp` where `date` = '$date' and `status` = '0'");
            $arr = array();

            foreach ($res as $request) {
                $request["userinfo"] = getUserInfo($request["uid"]);
                array_push($arr, $request);
            }

            $this->code = 200;
            $this->data = $arr;
        }

        public function order($date, $startblock, $endblock, $rid) {
            $uid = $this->user["id"];
            $tempOrder = $this->user["tempOrder"];

            $nowDate = date("Y-m-d");

            if ($tempOrder != 1 && (strtotime($date) < strtotime($nowDate) || (strtotime($date) == strtotime($nowDate) && ($startblock < 17 || date("H") > 11)))) {
                $this->code = 403;
                return;
            }

            if (strtotime($date) == strtotime($nowDate) && $startblock < $this->getTimeBlock() + 1) {
                $this->code = 403;
                return;                
            }

            $roomInfo = db::selectFirst("SELECT * FROM `app_rooms` where `id` = '$rid'");

            if (!$roomInfo) {
                $this->code = 404;
                return;
            }

            if ($roomInfo["special"] == 1) {
                $this->checkPermission("roomOrderSpecial");
            }

            $query_room_order = db::selectAll("SELECT * FROM `app_order` where `rid` = '$rid' and `date` = '$date'");

            foreach ($query_room_order as $order) {
                if ($this->isConflict($order, $startblock, $endblock)) {
                    $this->code = 306;
                    return;
                }
            }

            db::query("insert into `app_order` (`uid`, `rid`, `date`, `startBlock`, `endBlock`) VALUES ('$uid', '$rid', '$date', '$startblock', '$endblock'); ");

            $this->code = 200;
        }

        public function cancelOrder($id) {
            $uid = $this->user["id"];

            $order = db::selectFirst("SELECT * FROM `app_order` where `id` = '$id' and `uid` = '$uid'");

            if ($order["id"] != $id) {
                $this->code = 307;
                return;
            }

            db::query("DELETE FROM `app_order` where `id` = '$id'");
            $this->code = 200;
        }

        public function queryDateOrder($date) {
            $arr = array();

            $orderlist = db::selectAll("SELECT * FROM `app_order` where `date` = '$date' order by rid, startblock asc");

            foreach ($orderlist as $order) {
                $rid = $order["rid"];
                if (!array_key_exists($rid, $arr)) {
                    $roomInfo = db::selectFirst("SELECT * FROM `app_rooms` where `id` = '$rid'");
                    
                    $arr[$rid] = array(
                        "minBlock" => 10000,
                        "maxBlock" => -1,
                        "roomInfo" => $roomInfo
                    );
                }

                if ($order["startBlock"] < $arr[$rid]["minBlock"]) {
                    $arr[$rid]["minBlock"] = $order["startBlock"];
                }
                if ($order["endBlock"] > $arr[$rid]["maxBlock"]) {
                    $arr[$rid]["maxBlock"] = $order["endBlock"];
                }
    
            }

            $this->code = 200;
            $this->data = $arr;
        }

        public function queryRoomOrder($date, $id) {
            $roomInfo = db::selectFirst("SELECT * FROM `app_rooms` where `id` = '$id'");

            $arr = array();

            $orderlist = db::selectAll("SELECT * FROM `app_order` WHERE `date` = '$date' and `rid` = '$id' order by `date`, `startblock` asc");

            foreach ($orderlist as $order) {
                $order["userInfo"] = getUserInfo($order["uid"]);
                array_push($arr, $order);
            }

            $this->code = 200;
            $this->data = array(
                "data" => $arr,
                "roomInfo" => $roomInfo
            );
        }

        public function updateBooklist($id, $isbn, $libraryID, $name, $author) {
            if (empty($isbn) || empty($libraryID)) {
                $this->code = 308;
                return;
            }

            if (db::num_rows("SELECT * FROM `app_booklist` where `libraryID` = '$libraryID'")) {
                $this->code = 309;
                return;
            }

            if (!empty($name) || !empty($author)) {
                $edit = 1;
            } else {
                $edit = 0;
            }

            $bookInfo = db::selectFirst("SELECT * FROM `app_booklist_unconfirmed` where `id` = '$id'");

            $origin_name = $bookInfo["name"];
            $origin_author = $bookInfo["author"];
            $origin_publish = $bookInfo["publish"];
            $origin_cnt = $bookInfo["cnt"] - 1;

            db::query("INSERT INTO `app_booklist` (`libraryID`, `isbn`, `name`, `author`, `publish`, `edit_name`, `edit_author`, `edit`) VALUES ('$libraryID', '$isbn', '$origin_name', '$origin_author', '$origin_publish', '$name', '$author', '$edit')");
            db::query("UPDATE `app_booklist_unconfirmed` set `cnt` = '$origin_cnt' where `id` = '$id'");

            $this->code = 200;
        }

        private function getBookPreviewImgURL($isbn, $largeImg) {
            if ($largeImg) {
                if ($isbn < 1000000000) {
                    return IMG_URL."/images/tianyou/default.jpg?x-oss-process=image/auto-orient,1/resize,m_lfit,w_330/quality,q_90";
                } else {
                    return IMG_URL."/images/tianyou/".$isbn.".jpg?x-oss-process=image/auto-orient,1/resize,m_lfit,w_330/quality,q_90";
                }
            } else {
                if ($isbn < 1000000000) {
                    return IMG_URL."/images/tianyou/default.jpg?x-oss-process=image/auto-orient,1/resize,m_lfit,w_150/quality,q_90";
                } else {
                    return IMG_URL."/images/tianyou/".$isbn.".jpg?x-oss-process=image/auto-orient,1/resize,m_lfit,w_150/quality,q_90";
                }
            }
        }

        private function getBookExtendInfo($book, $largeImg = false) {
            $book["img"] = $this->getBookPreviewImgURL($book["isbn"], $largeImg);

            if (!empty($book["edit_name"])) {
                $book["name"] = $book["edit_name"];
            }

            if (!empty($book["edit_author"])) {
                $book["author"] = $book["edit_author"];
            }

            return $book;
        }

        private function queryNewestBookList() {
            $arr = array();

            $booklist =  db::selectAll("SELECT `id`,`isbn`,`callNumber`,`name`,`author`,`publish`,`edit_name`, `edit_author`, `storageTime` FROM `app_booklist` order by `storageTime` desc limit 10");

            $rand_id = array_rand($booklist, 3);

            foreach ($rand_id as $index) {
                $book = $this->getBookExtendInfo($booklist[$index]);
                array_push($arr, $book);
            }

            return $arr;
        }

        private function queryRandomBookList() {
            $arr = array();
            $booklist =  db::selectAll("SELECT `id`,`isbn`,`callNumber`,`name`,`author`,`publish`,`edit_name`, `edit_author`, `storageTime` FROM `app_booklist` order by RAND() limit 9");
            
            foreach ($booklist as $book) {
                $book = $this->getBookExtendInfo($book);
                array_push($arr, $book);
            }

            return $arr;
        }

        public function queryBookIndex() {
            $this->code = 200;
            $this->data = array(
                "newest" => $this->queryNewestBookList(),
                "random" => $this->queryRandomBookList()
            );
        }

        public function queryBook($keyword) {
            $arr = array();
            $booklist = db::selectAll("SELECT `id`,`isbn`,`callNumber`,`name`,`author`,`publish`,`edit_name`, `edit_author`, `storageTime` FROM `app_booklist` WHERE (`callNumber` like '%$keyword%' or `name` like '%$keyword%' or `author` like '%$keyword%' or `publish` like '%$keyword%') LIMIT 20");

            foreach ($booklist as $book) {
                $book = $this->getBookExtendInfo($book);
                array_push($arr, $book);                
            }

            $this->code = 200;
            $this->data = array(
                "booklist" => $arr
            );
        }

        public function queryBookInfo($id, $option) {
            if ($option == 1) {
                $book = db::selectFirst("SELECT * FROM `app_booklist` WHERE id = '$id'");

                if (!$book) {
                    $this->code = 310;
                    return;                    
                }

                $book = $this->getBookExtendInfo($book, true);

                $bookStatusOrigin = db::selectAll("SELECT * FROM `app_booklist_confirmed` WHERE `bookid` = '$id'");
                $bookStatus = array();

                foreach ($bookStatusOrigin as $status) {
                    $libraryID = $status["libraryID"];

                    $borrow = db::selectFirst("SELECT `uid`, `bookid` FROM `app_borrow` WHERE `libraryID` = '$libraryID' and `status` = '1'");

                    if (!$borrow) {
                        $status["reader"] = -1;
                    } else {
                        $status["reader"] = getUserInfo($borrow["uid"])["realname"];
                    }

                    array_push($bookStatus, $status);
                }

            } else {
                $bookStatus = db::selectFirst("SELECT * FROM `app_booklist_confirmed` WHERE libraryID = '$id'");

                if (!$bookStatus) {
                    $this->code = 310;
                    return;
                }

                $book = db::selectFirst("SELECT * FROM `app_booklist` WHERE id = '".$bookStatus["bookid"]."'");
                $book = $this->getBookExtendInfo($book, false);
            }

            $this->code = 200;
            $this->data = array(
                "bookInfo" => $book,
                "bookStatus" => $bookStatus
            );
        }

        public function insertUnconfirmedBook($libraryID) {
            if (db::num_rows("SELECT * FROM `app_booklist_unconfirmed` where `libraryID` = '$libraryID'")) {
                $this->code = 309;
                return;
            }

            if (db::num_rows("SELECT * FROM `app_booklist_confirmed` where `libraryID` = '$libraryID'")) {
                $this->code = 309;
                return;
            }

            db::query("INSERT INTO `app_booklist_unconfirmed` (`libraryID`) VALUES ('$libraryID')");

            $this->code = 200;
        }


        public function borrowBook($libraryID) {
            $uid = $this->user["id"];

            $bookStatus = db::selectFirst("SELECT * FROM `app_booklist_confirmed` WHERE `libraryID` = '$libraryID' and `status` = '1'");

            if (!$bookStatus) {
                $this->code = 311;
                return;
            }

            db::query("UPDATE `app_booklist_confirmed` SET `status` = '0' where `libraryID` = '$libraryID'");

            $bookid = $bookStatus["bookid"];
            $startDate = date("Y-m-d");
            $endDate = date("Y-m-d", strtotime('+60 day'));

            db::query("INSERT INTO `app_borrow` (`uid`, `bookid`, `libraryID`, `startDate`, `endDate`, `status`) VALUES ('$uid', '$bookid', '$libraryID', '$startDate', '$endDate', '1')");

            $this->code = 200;
        }

        public function returnBook($libraryID) {
            $uid = $this->user["id"];

            $bookStatus = db::selectFirst("SELECT * FROM `app_borrow` where `libraryID` = '$libraryID' and `status` = '1'");

            if (!$bookStatus) {
                $this->code = 312;
                return;
            }

            if ($bookStatus["uid"] != $uid) {
                $this->code = 313;
                return;                
            }

            db::query("UPDATE `app_booklist_confirmed` SET `status` = '1' where `libraryID` = '$libraryID'");

            db::query("UPDATE `app_borrow` SET `status` = '0' where `libraryID` = '$libraryID'");

            $this->code = 200;
        }

        public function print() {
            echo json_encode(array(
                "code" => $this->code,
                "data" => $this->data,
                "userinfo" => $this->user
            ));
        }
    }
?>