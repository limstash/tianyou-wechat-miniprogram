<?php
    frame::httpCode(403);
?>

<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,viewport-fit=cover">
    <title>验证失败</title>
    <link rel="stylesheet" href="/static/css/weui.css">
    <style id="hcSearchePopoverCustomStyle"></style>
</head>
<body ontouchstart>
    <div class="container" id="container">
    <div class="page msg_success js_show">
    <div class="weui-msg">
        <div class="weui-msg__icon-area"><i class="weui-icon-warn weui-icon_msg"></i></div>
        <div class="weui-msg__text-area">
            <h2 class="weui-msg__title">验证失败</h2>
            <p class="weui-msg__desc">您的验证码已过期</p>
        </div>
    </div>
    </div>
    </div>
</body>
</html>