<?php

    /**
     * 执行库：地址转发 (lib/lib_controller.php)
     */

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    ob_clean();
    ob_start();

    $html = new HTML();

    $html->route("/verify", "verify", NULL, 1);
    $html->route("/verify/(\S+)", "verify_email", NULL, 1);
    $html->route("/resend", "resend", NULL, 1);

    $html->route("/userinfo", "user_info", NULL, 1);
    $html->route("/verifyinfo", "verify_info", NULL, 1);

    $html->route("/login", "login", NULL, 1);
    $html->route("/search", "search", NULL, 1);
    
    $html->route("/order", "order", NULL, 1);
    $html->route("/order/cancel", "order_cancel", NULL, 1);
    $html->route("/order/temp", "order_temp", NULL, 1);
    $html->route("/order/templist", "order_templist", NULL, 1);
    $html->route("/orderlist", "orderlist", NULL, 1);
    
    $html->route("/admin/order/date", "admin_order_date", NULL, 1);
    $html->route("/admin/order/room", "admin_order_room", NULL, 1);
    $html->route("/admin/order/temp", "admin_order_temp", NULL, 1);

    $html->route("/booklist", "booklist", NULL, 1);
    $html->route("/booklist/unconfirmed", "booklist_unconfirmed", NULL, 1);
    $html->route("/booklist/index", "booklist_index", NULL, 1);
    $html->route("/booklist/search", "booklist_search", NULL, 1);
    $html->route("/booklist/detail", "booklist_detail", NULL, 1);
    $html->route("/booklist/borrow", "booklist_borrow", NULL, 1);

    $html->route("/403", "403", "403", 0);
    $html->route("/404", "404", "404", 0);
    
    $html->route("/verify_status/success", "verify_success", "验证成功", 0);
    $html->route("/verify_status/invalid", "verify_invalid", "验证失败", 0);
    $html->route("/verify_status/retired", "verify_retired", "验证失败", 0);

    $html->checkRoute();

    $html->appendFlag = 0;

    $html->printHTML();

    ob_flush();
    ob_end_flush();
?>