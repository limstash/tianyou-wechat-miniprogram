<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $id = db::escape($_POST["id"]);
    $option = db::escape($_POST["option"]);

    $client = new client(false);

    $client->queryBookInfo($id, $option);
    $client->print();
?>