<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $nickname = db::escape($_POST["nickname"]);

    $client = new client(true, $token);
    $client->resendVerifyEmail($nickname);

    $client->print();
?>