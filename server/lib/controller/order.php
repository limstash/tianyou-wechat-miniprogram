<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $startblock = db::escape($_POST["startblock"]) + 1;
    $endblock = db::escape($_POST["endblock"]) + 1;
    $year = db::escape($_POST["year"]);
    $month = db::escape($_POST["month"]);
    $day = db::escape($_POST["day"]);
    $rid = db::escape($_POST["rid"]);
    $date = $year."-".$month."-".$day;

    $client = new client(true, $token);
    $client->checkPermission("roomOrder");
    
    $client->order($date, $startblock, $endblock, $rid);
    $client->print();
?>