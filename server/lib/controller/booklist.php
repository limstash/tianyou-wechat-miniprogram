<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);

    $client = new client(true, $token);
    $client->getNotReturnBook();
    $client->print();
?>