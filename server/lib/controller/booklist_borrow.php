<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $libraryID = db::escape($_POST["libraryID"]);
    $token = db::escape($_POST["token"]);
    $option = db::escape($_POST["option"]);

    $client = new client(true, $token);
    $client->checkPermission("bookBorrow");
    
    if ($option == 0) {
        $client->returnBook($libraryID);
    } else {
        $client->borrowBook($libraryID);
    }

    $client->print();
?>