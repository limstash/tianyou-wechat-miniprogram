<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $libraryID = db::escape($_POST["libraryID"]);

    $client = new client(true, $token);
    $client->checkPermission("manageBook");

    $client->insertUnconfirmedBook($libraryID);
    $client->print();
?>