<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $date = db::escape($_POST["date"]);

    $client = new client(true, $token);
    $client->checkPermission("manageOrder");

    $client->queryDateOrder($date);
    $client->print();
?>