<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $pregResult = preg_match_all("/\/verify\/(\S+)/", $_SERVER['REQUEST_URI'], $pageArray);

    if(!$pregResult){
        header("Location:/404");
        exit;
    }else{
        $token = $pageArray[1][0];
    }

    $verifyInfo = db::selectFirst("select * from `app_verify_token` where `token` = '$token'");

    if (!$verifyInfo) {
        header("Location:/verify_status/invalid");
        exit;
    }

    if ($verifyInfo["used"] == 1) {
        header("Location:/verify_status/retired");
        exit;    
    }

    $uid = $verifyInfo["uid"];
    $gid = $verifyInfo["gid"];
    $ecjtuID = $verifyInfo["ecjtuID"];
    $realname = $verifyInfo["realname"];

    db::query("update `app_users` SET `gid` = '$gid' where `id` = '$uid'");
    db::query("update `app_users` SET `status` = '2' where `id` = '$uid'");
    db::query("update `app_users` SET `ecjtuID` = '$ecjtuID' where `id` = '$uid'");
    db::query("update `app_users` SET `realname` = '$realname' where `id` = '$uid'");
    db::query("update `app_verify_token` SET `used` = '1' where `token` = '$token'");

    header("Location:/verify_status/success");
    exit;
?>