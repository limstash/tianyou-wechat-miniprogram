<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $code = $_GET["code"];
    
    $client = new client(false);
    $client->login($code);
    $client->print();

?>