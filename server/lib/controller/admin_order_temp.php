<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $id = db::escape($_POST["id"]);

    $client = new client(true, $token);
    $client->checkPermission("manageOrder");

    $client->updateTempOrderRequest($id);
    $client->print();
?>