<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $keyword = db::escape($_POST["keyword"]);
    $client = new client(false);

    $client->queryBook($keyword);
    $client->print();
?>