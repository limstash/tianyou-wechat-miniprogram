<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $id = db::escape($_POST["id"]);
    $realname = db::escape($_POST["realname"]);
    $nickname = $_POST["nickname"];

    $client = new client(true, $token);
    $token = $client->verify($id, $realname);

    if (!empty($token)) {
        $client->sendVerifyMail($id, $nickname, $token);
    }

    $client->print();
?>