<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $client = new client(false);
    $client->queryBookIndex();

    $client->print();
?>