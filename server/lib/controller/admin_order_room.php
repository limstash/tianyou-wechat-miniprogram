<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $date = db::escape($_POST["date"]);
    $rid = db::escape($_POST["id"]);

    $client = new client(true, $token);
    $client->checkPermission("manageOrder");

    $client->queryRoomOrder($date, $rid);
    $client->print();
?>