<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $token = db::escape($_POST["token"]);
    $id = db::escape($_POST["id"]);

    $client = new client(true, $token);
    $client->checkPermission("roomOrder");
    
    $client->cancelOrder($id);

    $client->print();
?>