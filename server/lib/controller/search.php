<?php

    if(!defined("load")){
        header("Location:/403");
        exit;
    }

    $type = db::escape($_POST["type"]) + 1;
    $startblock = db::escape($_POST["startblock"]) + 1;
    $endblock = db::escape($_POST["endblock"]) + 1;
    $year = db::escape($_POST["year"]);
    $month = db::escape($_POST["month"]);
    $day = db::escape($_POST["day"]);
    $date = $year."-".$month."-".$day;
    $token = db::escape($_POST["token"]);

    $client = new client(true, $token);
    $client->search($type, $startblock, $endblock, $date);
    $client->print();
?>