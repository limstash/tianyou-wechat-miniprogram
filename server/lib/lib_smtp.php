<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require PATH.'/3rdpart/phpmailer/Exception.php';
    require PATH.'/3rdpart/phpmailer/PHPMailer.php';
    require PATH.'/3rdpart/phpmailer/SMTP.php';

    function sendMail($target, $title, $text) {
        $mail = new PHPMailer(true); 

        $mail->CharSet ="UTF-8";
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = smtp_server;
        $mail->SMTPAuth = true;
        $mail->Username = smtp_username;
        $mail->Password = smtp_password;
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom(smtp_username, '');
        $mail->addAddress($target, '');
        
        $mail->isHTML(true);
        $mail->Subject = $title;
        $mail->Body    = $text;
        $mail->AltBody = '';

        $mail->send();
    }
?>