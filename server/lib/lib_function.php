<?php

    /**
     * 执行库 : 常用函数定义 (/lib/lib_function.php)
     */

    if(!defined("load")){
        header("Location:/403");
        exit;
    }
    
    /**
     * 发送外部请求
     * 
     * @param string $url
     * @param string $method
     * @param array $requestData
     * 
     * @return string $result
     */
    
	function request($url, $method, $requestData=array()){
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($requestData));
        }

        $res = curl_exec($curl);
        curl_close($curl);

        return $res;
    }

    function getGroupPermission($gid) {
        return db::selectFirst("SELECT * FROM `app_usergroup` where `id` = '$gid'");
    }

    function getTempOrderPermission($uid) {
        $date = date("Y-m-d");
        $res = db::selectFirst("SELECT * FROM `app_order_temp` where `uid` = '$uid' and `date` = '$date'");

        if (!isset($res["status"])) {
            return -1;
        } else {
            return $res["status"];
        }
    }

    function getUserInfo($uid) {
        $res = db::selectFirst("SELECT * FROM `app_users` where `id` = '$uid'");
        $res["permission"] = getGroupPermission($res["gid"]);
        $res["tempOrder"] = getTempOrderPermission($uid);
        return $res;
    }

    function getUserInfoByKeywords($keywords, $data) {
        $res =  db::selectFirst("SELECT * FROM `app_users` where `{$keywords}` = '$data'");
        $uid = $res["id"];
        $res["permission"] = getGroupPermission($res["gid"]);
        $res["tempOrder"] = getTempOrderPermission($uid);
        return $res;
    }


?>