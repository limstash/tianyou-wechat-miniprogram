<?php

    error_reporting(E_ERROR | E_PARSE);

    date_default_timezone_set('PRC');

    session_set_cookie_params(30 * 24 * 3600); 
    session_start();

    define("load", true);
    define("PATH", dirname(__FILE__));
    define("MININUM_FRAMRWORK_VERSION", "3.1");
    define("VERSION", "1.0");
    
    if(!include_once("lib/init_load.php")){
        exit("Oops: Failed to execute init target - No such file");
    }
?>