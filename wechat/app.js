const CONFIG = require('config.js')
App({
  onLaunch: function () {

  },
  login : function () {
    let that = this;
    let promise = new Promise((resolve, reject)=>{
      wx.login({
        success: function (res) {
          wx.request({
            url: 'https://'+CONFIG.domain+'/login',
            data: {
              code: res.code
            },
            success: function(res) {
              wx.hideLoading();
              if (res.data.code != 200) {
                wx.showModal({
                  title: '提示',
                  content: '登录失败，请重试',
                  showCancel:false
                })
                reject()
              }
              that.globalData.uid = res.data.data.uid;
              that.globalData.token = res.data.data.token;
              that.globalData.status = res.data.data.status;
              that.globalData.permission = res.data.userinfo.permission;
              that.globalData.tempOrder = res.data.userinfo.tempOrder;

              that.getUserInfo().then((res) => {
                resolve();
              })
            }
          })
        }
      })
    })
    return promise
  },
  getUserInfo:function() {
    let that = this;
    let promise = new Promise((resolve, reject)=>{
      wx.getUserInfo({
        success:(data) =>{
          that.globalData.userInfo = data.userInfo;
          resolve();
        }
      })
    })
    return promise
  },
  globalData:{
    userInfo:null,
    available: null,
    busy: null,

    uid:-1,
    token:-1,
    status:-1,
    permission: null,
    tempOrder: -1,

    year:-1,
    month:-1,
    day:-1,
    startblock:-1,
    endblock:-1
  }
})