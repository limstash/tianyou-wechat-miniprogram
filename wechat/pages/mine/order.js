// miniprogram/pages/mine/order.js
const App = getApp()
const CONFIG = require('../../config.js')
Page({
  mixins: [require('../../libs/weui/themeChanged')],

  /**
   * 页面的初始数据
   */
  data: {
    list: null,
    id: "",
    name: "",
    date: "",
    dialog: false,
    empty: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      array1: CONFIG.roomFullCategory,
      array2: CONFIG.timeTable,
    })

    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })
    
    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/orderlist',
      method: "POST",
      data: {
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '拉取预约信息失败',
            showCancel:false
          })
          return;
        }
        that.setData({
          list: res.data.data.data
        })

        if (res.data.data.data.length == 0) {
          that.setData({
            empty: true
          })
        }
      }
    })
  },
  cancel: function(event) {
    var id = event.currentTarget.dataset.index
    var name = event.currentTarget.dataset.name
    var date = event.currentTarget.dataset.date

    this.setData({
      dialog: true,
      id: id,
      roomname: name,
      date: date
    });
  },
  close: function () {
    this.setData({
        dialog: false,
    })
  },

  cancelRequest: function () {
    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/order/cancel',
      method: "POST",
      data: {
        token:App.globalData.token,
        id: that.data.id
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '撤销失败',
            showCancel:false
          })
        }   
        that.setData({
          dialog: false,
        })       
        that.onLoad()  
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})