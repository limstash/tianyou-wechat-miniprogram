const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    room: 0,
    book: 0,
    userInfo: {},
    verifyPath: null,
    status: "未认证",

    onLoad: false,
    dialog: false
  },

  syncUserInfo: function (data) {
    if (data == null) {
      return
    }
    App.globalData.status = data.status;
    App.globalData.permission = data.permission;
    App.globalData.tempOrder = data.tempOrder;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      version: CONFIG.version
    })
    wx.stopPullDownRefresh()
    
    wx.showLoading({
      title: '加载中',
    })
    
    App.login().then((res) => {
      wx.checkSession({
        fail: function(res){
          App.login()　　
        }
      })

      this.setData({
        permission: App.globalData.permission
      })

      var that = this;
      if (!App.globalData.userInfo) {
        wx.redirectTo({
          url: '../authorization/index?backType=mine'
        })
      }
      that.setData({
        userInfo : App.globalData.userInfo
      })
      that.setData({
        verifyPath : (App.globalData.status == "0") ? '/pages/authorization/identity' : '/pages/authorization/mailsend'
      })
      that.setData({
        permission: App.globalData.permission
      })

      wx.request({
        url: 'https://'+CONFIG.domain+'/userinfo',
        method: "POST",
        data: {
          token:App.globalData.token
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          wx.hideLoading()
          if (res.data.code != 200) {
            wx.showModal({
              title: '提示',
              content: '获取信息失败',
              showCancel:false
            })
          }   
          that.setData({
            room: res.data.data.orderCount,
            book: res.data.data.bookCount
          })       

          that.syncUserInfo(res.data.userinfo)
        }
      })

      if (App.globalData.status == 1) {
        this.setData({
          status: "待认证"
        })
      }else if(App.globalData.status == 2) {
        this.setData({
          status: "已认证"
        })      
      }

      this.setData({
        onLoad: true
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.onLoad) {
      this.setData({
        permission: App.globalData.permission
      })

      var that = this
      wx.request({
        url: 'https://'+CONFIG.domain+'/userinfo',
        method: "POST",
        data: {
          token:App.globalData.token
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          if (res.data.code != 200) {
            wx.showModal({
              title: '提示',
              content: '获取信息失败',
              showCancel:false
            })
          }   
          that.setData({
            room: res.data.data.orderCount,
            book: res.data.data.bookCount
          })       

          that.syncUserInfo(res.data.userinfo)
        }
      })

      if (App.globalData.status == 1) {
        this.setData({
          status: "待认证"
        })
      }else if(App.globalData.status == 2) {
        this.setData({
          status: "已认证"
        })      
      }
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  close: function () {
    this.setData({
        dialog: false
    })
  },

  tempOrder(){
    if (App.globalData.tempOrder == 1) {
      wx.showModal({
        title: '提示',
        content: '您的临时预约权限已开通',
        showCancel:false
      })
    } else if (App.globalData.tempOrder == 0) {
      wx.showModal({
        title: '提示',
        content: '您的临时预约权限申请已提交',
        showCancel:false
      })    
    } else {
      this.setData({
        dialog: true
      });
    }
  },

  confirm() {
    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/order/temp',
      method: "POST",
      data: {
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        that.close()

        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '申请失败',
            showCancel:false
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '申请成功',
            showCancel:false
          })
          that.onShow()
        }
      }
    })
  },

  borrow(){
    if (this.data.permission.bookBorrow != 1) {
      wx.showModal({
        title: '提示',
        content: '您的借阅权限已被关闭',
        showCancel:false
      })
      return
    }

    wx.scanCode({
      onlyFromCamera: true,
      scanType: ["barCode"],
      success(res) {
        wx.navigateTo({
          url: '/pages/book/borrow?id='+res.result,
        })
      },
      fail(err) {
        console.error(err)
        wx.showToast({
          title: err.errMsg,
          icon: 'none'
        })
      }
    })
  },
})