const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bookinfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })
    
    this.setData({
      permission: App.globalData.permission
    })

    if (this.data.permission.manageBook != 1) {
      wx.showModal({
        title: '提示',
        content: '您无权限访问此页面',
        showCancel:false
      })
      wx.navigateBack()
      return
    }
  },
  scanLibraryCode(){
    var that = this
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ["barCode"],
      success(res) {
        that.setData({
          libraryID : res.result
        })
      },
      fail(err) {
        console.error(err)
        wx.showToast({
          title: err.errMsg,
          icon: 'none'
        })
      }
    })
  },
  scanLibraryCodeCheck(){
    var that = this
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ["barCode"],
      success(res) {
        that.setData({
          check_libraryID : res.result
        })
      },
      fail(err) {
        console.error(err)
        wx.showToast({
          title: err.errMsg,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  showDialog(msg) {
    wx.hideLoading();
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel:false
    })
  },
  bindSave: function(event) {
    var value = event.detail.value
    if (value.libraryID == "") {
      this.showDialog("图书馆编号不能为空")
    } else if (value.check_libraryID == "") {
      this.showDialog("图书馆编号未验证")
    } else if (value.libraryID != value.check_libraryID) {
      this.showDialog("图书馆编号校验失败")
    } else {
      var that = this
      wx.request({
        url: 'https://'+CONFIG.domain+'/booklist/unconfirmed',
        method: "POST",
        data: {
          token:App.globalData.token,
          libraryID: value.libraryID,
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          if (res.data.code == 309) {
            that.showDialog("该编号已被入库")
          } else if (res.data.code != 200) {
            wx.showModal({
              title: '提示',
              content: '提交失败',
              showCancel:false
            })
          } else {
            that.showDialog("入库成功")
          }
          wx.navigateBack()
        }
      })
    }
  }
})