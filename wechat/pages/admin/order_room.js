const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: null,
    cid: "",
    name: "",
    location: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      array1: CONFIG.roomFullCategory,
      array2: CONFIG.timeTable,
    })
    
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })
    
    this.setData({
      permission: App.globalData.permission
    })

    if (this.data.permission.manageOrder != 1) {
      wx.showModal({
        title: '提示',
        content: '您无权限访问此页面',
        showCancel:false
      })
      wx.navigateBack()
      return
    }
    
    wx.showLoading({
      title: '加载中',
    })
    
    var date = options.date
    var id = options.id
    var that = this

    wx.request({
      url: 'https://'+CONFIG.domain+'/admin/order/room',
      method: "POST",
      data: {
        token:App.globalData.token,
        date: date,
        id: id
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '拉取预约信息失败',
            showCancel:false
          })
          return;
        }
        that.setData({
          list: res.data.data.data,
          cid: res.data.data.roomInfo.cid,
          name: res.data.data.roomInfo.name,
          location: res.data.data.roomInfo.location
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})