const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dateArr: null,
    dialog: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  getDateStr: function(day) {
    var date = new Date();
    date.setDate(date.getDate() + day);//获取AddDayCount天后的日期 
      var y = date.getFullYear();
      var m = date.getMonth() + 1;//获取当前月份的日期 
      var d = date.getDate();
      if(m < 10){
        m = '0' + m;
      };
      if(d < 10) {
        d = '0' + d;
      };
 
      return y + "-" + m + "-" + d;
  },

  onLoad: function (options) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })
    
    this.setData({
      permission: App.globalData.permission
    })

    if (this.data.permission.manageOrder != 1) {
      wx.showModal({
        title: '提示',
        content: '您无权限访问此页面',
        showCancel:false
      })
      wx.navigateBack()
      return
    }

    wx.showLoading({
      title: '加载中',
    })

    var dateArr = []
    for (var i=0; i<7;i++) { 
      dateArr.push(this.getDateStr(i))
    }
    this.setData({
      dateArr: dateArr
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/order/templist',
      method: "POST",
      data: {
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '拉取权限申请信息失败',
            showCancel:false
          })
          return;
        }
        that.setData({
          tempOrder: res.data.data
        })
      }
    })
  },
  tap: function (event) {
    var date = event.currentTarget.dataset.index
    wx.navigateTo({
      url: './order_date?date='+date,
    })
  },
  close: function () {
    this.setData({
        dialog: false
    })
  },
  permission: function (event) {
    var id = event.currentTarget.dataset.index
    var realname = event.currentTarget.dataset.realname

    this.setData({
      tempOrderRequestID: id,
      tempOrderRequestName: realname,
      dialog: true,
    })
  },
  confirm: function (event) {
    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/admin/order/temp',
      method: "POST",
      data: {
        token:App.globalData.token,
        id: that.data.tempOrderRequestID
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        that.close()

        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '操作失败',
            showCancel:false
          })
        } else {
          that.onLoad()
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})