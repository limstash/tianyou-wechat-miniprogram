const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    available: "",
    busy : "",
    cid: "",
    dialog: false,
    dialog2: false,
    rid: "",
    roomname: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      permission: App.globalData.permission
    })
    
    this.setData({
      cid: options.cid,
      available: App.globalData.available,
      busy:App.globalData.busy
    })

    this.setData({
      array1: CONFIG.roomFullCategory
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  close: function () {
    this.setData({
        dialog: false,
        dialog2: false,
    })
  },

  order: function(event) {
    var rid = event.currentTarget.dataset.index
    var name = event.currentTarget.dataset.name

    this.setData({
      dialog: true,
      rid: rid,
      roomname: name
    });
  },
  confirm:function(event) {
    this.setData({
      dialog: false,
      dialog2: true
    });
  },
  request: function(event) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })

    wx.showLoading({
      title: '加载中',
    })
    
    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/order',
      method: "POST",
      data: {
        token:App.globalData.token,
        year: App.globalData.year,
        month: App.globalData.month,
        day: App.globalData.day,
        startblock: App.globalData.startblock,
        endblock : App.globalData.endblock,
        rid: that.data.rid
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        
        if (res.data.code == 200) {
          wx.redirectTo({
            url: '../mine/order'
          })
        } else if (res.data.code == 306) {
          wx.showModal({
            title: '提示',
            content: '已经被他人预约，请选择其他教室',
            showCancel:false
          })
          that.setData({
            dialog: false,
          })     
        } else {
          wx.showModal({
            title: '提示',
            content: '预约失败',
            showCancel:false
          })
        }
      }
    })
  }
})