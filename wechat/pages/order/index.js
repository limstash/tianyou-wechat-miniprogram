const App = getApp()
const CONFIG = require('../../config.js')
Page({
  mixins: [require('../../libs/weui/themeChanged')],
  data : {
    calendarConfig: {
      multi: false, // 是否开启多选,
      weekMode: false, // 周视图模式
      theme: 'elegant', // 日历主题，目前共两款可选择，默认 default 及 elegant，自定义主题在 theme 文件夹扩展
      showLunar: false, // 是否显示农历，此配置会导致 setTodoLabels 中 showLabelAlways 配置失效
      inverse: true, // 单选模式下是否支持取消选中,
      chooseAreaMode: false, // 开启日期范围选择模式，该模式下只可选择时间段
      markToday: '今', // 当天日期展示不使用默认数字，用特殊文字标记
      defaultDay: false, // 默认选中指定某天；当为 boolean 值 true 时则默认选中当天，非真值则在初始化时不自动选中日期，
      highlightToday: false, // 是否高亮显示当天，区别于选中样式（初始化时当天高亮并不代表已选中当天）
      takeoverTap: false, // 是否完全接管日期点击事件（日期不会选中），配合 onTapDay() 使用
      preventSwipe: false, // 是否禁用日历滑动切换月份
      firstDayOfWeek: 'Mon', // 每周第一天为周一还是周日，默认按周日开始
      onlyShowCurrentMonth: false, // 日历面板是否只显示本月日期
      hideHeadOnWeekMode: false, // 周视图模式是否隐藏日历头部
      showHandlerOnWeekMode: true, // 周视图模式是否显示日历头部操作栏，hideHeadOnWeekMode 优先级高于此配置
      disableMode: {  // 禁用某一天之前/之后的所有日期
        type: 'before',  // [‘before’, 'after']
        date: '2002-09-05', // 无该属性或该属性值为假，则默认为当天
      }
    },
    date : "请选择",
    value1: 0,
    value2: 0,
    value3: 0,
    default1: "请选择",
    default2: "请选择",
    default3: "请选择",
    year : "",
    month: "",
    day : "",

    onLoad: false
  },
  bindPicker1Change: function(e) {
    this.setData({
        value1: e.detail.value,
        default1: this.data.array1[e.detail.value]
    })
  },

  bindPicker2Change: function(e) {
    this.setData({
        value2: e.detail.value,
        default2: this.data.array2[e.detail.value]
    })
  },

  bindPicker3Change: function(e) {
    this.setData({
        value3: e.detail.value,
        default3: this.data.array3[e.detail.value]
    })
  },

  close: function() {
    this.setData({
        dialog1: false,
    });
  },
  open1() {
    this.setData({
        dialog1: true
    });
  },
  /**
   * 选择日期后执行的事件
   * currentSelect 当前点击的日期
   * allSelectedDays 选择的所有日期（当multi为true时，allSelectedDays有值）
   */
  afterTapDay(e) {
    var month = e.detail.month
    if (month < 10) {
      month = '0' + month
    }
    var day = e.detail.day
    if (day < 10) {
      day = '0' + day
    }
    var date = e.detail.year+"-"+month+"-"+day
    this.setData({
      year: e.detail.year,
      month : e.detail.month,
      day: e.detail.day,
      date: date
    });
  },
  /**
   * 当日历滑动时触发(适用于周/月视图)
   * 可在滑动时按需在该方法内获取当前日历的一些数据
   */
  onSwipe(e) {
    const dates = this.calendar.getCalendarDates();
  },
  /**
   * 当改变月份时触发
   * => current 当前年月 / next 切换后的年月
   */
  whenChangeMonth(e) {
    // => { current: { month: 3, ... }, next: { month: 4, ... }}
  },
  /**
   * 周视图下当改变周时触发
   * => current 当前周信息 / next 切换后周信息
   */
  whenChangeWeek(e) {
    // {
    //    current: { currentYM: {year: 2019, month: 1 }, dates: [{}] },
    //    next: { currentYM: {year: 2019, month: 1}, dates: [{}] },
    //    directionType: 'next_week'
    // }
  },
  /**
   * 日期点击事件（此事件会完全接管点击事件），需自定义配置 takeoverTap 值为真才能生效
   * currentSelect 当前点击的日期
   */
  onTapDay(e) {

  },
  /**
   * 日历初次渲染完成后触发事件，如设置事件标记
   */
  afterCalendarRender(e) {

  },
  showDialog(msg) {
    wx.hideLoading();
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel:false
    })
  },
  syncUserInfo: function (data) {
    if (data == null) {
      return
    }
    App.globalData.status = data.status;
    App.globalData.permission = data.permission;
    App.globalData.tempOrder = data.tempOrder;
  },
  search(e) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })

    if (this.data.permission.roomOrder != 1) {
      wx.showModal({
        title: '提示',
        content: '您的预约权限已被关闭',
        showCancel:false
      })
      return
    }

    wx.showLoading({
      title: '加载中',
    })
    
    var that = this;
    if (!App.globalData.userInfo) {
      wx.redirectTo({
        url: '../authorization/index?backType=order'
      })
      return
    }
  
    if (this.data.default1 == "请选择") {
      this.showDialog("类型未选择")
    } else if (this.data.date == "请选择") {
      this.showDialog("日期未选择")
    } else if (this.data.default2 == "请选择") {
      this.showDialog("开始时间未选择")
    } else if (this.data.default3 == "请选择") {
      this.showDialog("结束时间未选择")
    } else if (parseInt(this.data.value2) >= parseInt(this.data.value3)) {
      this.showDialog("开始时间不能晚于结束时间")
    } else if (App.globalData.tempOrder != 1 && this.data.date == this.getDateStr(0) && this.data.value2 < 16) {
      this.showDialog("当天预约不能早于12点，请申请临时预约权限")
    } else if (this.data.date == this.getDateStr(0) && this.data.value2 < this.getTimeBlock() + 1){
      this.showDialog("开始时间不能早于当前时间")
    } else {
      var that = this
      wx.request({
        url: 'https://'+CONFIG.domain+'/search',
        method: "POST",
        data: {
          token:App.globalData.token,
          type:that.data.value1,
          startblock: that.data.value2,
          endblock: that.data.value3,
          year : that.data.year,
          month: that.data.month,
          day : that.data.day,        
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          wx.hideLoading()

          if (res.data.code != 200) {
            wx.showModal({
              title: '提示',
              content: '拉取预约信息失败',
              showCancel:false
            })
            return;
          }

          that.syncUserInfo(res.data.userinfo)

          if (res.data.userinfo.status != "2") {
            wx.showModal({
              title: '提示',
              content: '请先完成用户身份认证',
              showCancel:false
            })
            return;            
          }

          App.globalData.year = that.data.year,
          App.globalData.month = that.data.month,
          App.globalData.day = that.data.day,
          App.globalData.startblock = that.data.value2,
          App.globalData.endblock = that.data.value3,

          App.globalData.available = res.data.data.available
          App.globalData.busy = res.data.data.busy

          var value1 = that.data.value1
          var url = "roomlist?cid="+value1

          wx.navigateTo({
            url: url
          });
        }
      })
    }
  },
  getDate: function(){
    const targetTimezone = -8;
    const dif = new Date().getTimezoneOffset();
    const east9time = new Date().getTime() + dif * 60 * 1000 - (targetTimezone * 60 * 60 * 1000);
    return new Date(east9time)
  },
  getTimeBlock: function() {
    var date = this.getDate()
    var hour = date.getHours()
    var minutes = date.getMinutes()
    var block = 4 * (hour - 8)
    if (minutes >= 45) {
      block += 3;
    } else if (minutes >= 30) {
      block += 2;
    } else if (minutes >= 15) {
      block += 1;
    }
    return block;
  },

  getDateStr: function(day) {
    var date = this.getDate();
    date.setDate(date.getDate() + day);//获取AddDayCount天后的日期 
      var y = date.getFullYear();
      var m = date.getMonth() + 1;//获取当前月份的日期 
      var d = date.getDate();
      if(m < 10){
        m = '0' + m;
      };
      if(d < 10) {
        d = '0' + d;
      };
 
      return y + "-" + m + "-" + d;
  },

  setCalendarStartDate() {
    var date = this.getDate()
    if (date.getHours() < 11 || App.globalData.tempOrder == 1) {
      var dayTextStart = this.getDateStr(0)
    } else {
      var dayTextStart = this.getDateStr(1)
    }
    var str = 'calendarConfig.disableMode.date'
    this.setData({
      [str] : dayTextStart
    })
  },

  onLoad: function () {
    var str = 'calendarConfig.disableMode.date'
    var dayTextStart = this.getDateStr(31)

    this.setData({
      [str] : dayTextStart
    })

    wx.showLoading({
      title: '加载中',
    })

    var that = this
    
    App.login().then((res) => {
      that.setData({
        permission: App.globalData.permission
      })

      that.setData({
        array1: CONFIG.roomCategory,
        array2: CONFIG.timeTable,
        array3: CONFIG.timeTable
      })
      
      if (that.data.permission.roomOrderSpecial == 1) {
        that.setData({
          array1:  CONFIG.roomFullCategory
        })
      }

      that.setData({
        onLoad: true
      })

      wx.hideLoading()
      that.setCalendarStartDate()
    })
  },
  
  onShareAppMessage: function () {

  },

  onShow: function() {
    if (this.data.onLoad) {
      this.setData({
        permission: App.globalData.permission
      })

      this.setData({
        array1: CONFIG.roomCategory
      })
      
      if (this.data.permission.roomOrderSpecial == 1) {
        this.setData({
          array1:  CONFIG.roomFullCategory
        })
      }

      this.setCalendarStartDate()
    }
  }
});