const App = getApp()
const CONFIG = require('../../config.js')

Page({
  data: {
    inputVal: "",
  },

  onLoad: function(e) {
    wx.stopPullDownRefresh()
    var that = this

    wx.showLoading({
      title: '加载中',
    })

    wx.request({
      url: 'https://'+CONFIG.domain+'/booklist/index',
      method: "GET",
      success: function(res) {
        wx.hideLoading()
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '获取信息失败',
            showCancel:false
          })
        }   
        that.setData({
          newest: res.data.data.newest,
          random: res.data.data.random
        })       
      }
    })
  },

  onShow: function(e){

  },

  bindinput(e) {
    this.setData({
      inputVal: e.detail.value
    })
  },

  bindconfirm(e) {
    this.setData({
      inputVal: e.detail.value
    })
    wx.navigateTo({
      url: '/pages/book/list?name=' + this.data.inputVal,
    })
  },
  goSearch(){
    wx.navigateTo({
      url: '/pages/book/list?name=' + this.data.inputVal,
    })
  },
  toDetailsTap(e) {
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/book/detail?id='+id,
    })
  },
  onShareAppMessage: function () {

  },

  onPullDownRefresh: function () {
    this.onLoad()
  },
})