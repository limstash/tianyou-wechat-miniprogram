const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    "html" : ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id: options.id,
    })

    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/booklist/detail',
      method: "POST",
      data: {
        id:that.data.id,
        option: 1
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '获取信息失败',
            showCancel:false
          })
        }   
        that.setData({
          bookInfo: res.data.data.bookInfo,
          bookStatus: res.data.data.bookStatus
        })       
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.bookInfo.name,
      imageUrl: this.data.bookInfo.img
    }
  }
})