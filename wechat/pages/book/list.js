const App = getApp()
const CONFIG = require('../../config.js')

Page({
  /**
   * 页面的初始数据
   */
  data: { 
    keyword: '', // 搜索关键词
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      keyword: options.name,
    })
    this.search()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  
  async search(){
    // 搜索商品
    wx.showLoading({
      title: '加载中',
    })

    var that = this

    wx.request({
      url: 'https://'+CONFIG.domain+'/booklist/search',
      method: "POST",
      data: {
        keyword:that.data.keyword
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code != 200) {
          wx.hideLoading();
          wx.showModal({
            title: '提示',
            content: '获取信息失败',
            showCancel:false
          })
        }   
        that.setData({
          booklist: res.data.data.booklist
        })       
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  bindinput(e){
    this.setData({
      keyword: e.detail.value
    })
  },
  bindconfirm(e){
    this.setData({
      keyword: e.detail.value
    })
    this.search()
  },
  goSearch(){
    this.setData({
      keyword: e.detail.value
    })
    this.search()
  },
  bindtap(e){
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/book/detail?id='+id,
    })
  }
})