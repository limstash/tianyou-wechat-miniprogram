const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  getDateStr: function(day) {
    var date = new Date();
    date.setDate(date.getDate() + day);//获取AddDayCount天后的日期 
      var y = date.getFullYear();
      var m = date.getMonth() + 1;//获取当前月份的日期 
      var d = date.getDate();
      if(m < 10){
        m = '0' + m;
      };
      if(d < 10) {
        d = '0' + d;
      };
 
      return y + "-" + m + "-" + d;
  },
    
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (App.globalData.status != "2") {
      wx.showModal({
        title: '提示',
        content: '请先完成用户身份认证',
        showCancel: false
      })
      return;            
    }

    var startDate = this.getDateStr(0)
    var endDate = this.getDateStr(60)
    this.setData({
      id: options.id,
      startDate: startDate,
      endDate: endDate
    })

    wx.showLoading({
      title: '加载中',
    })

    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/booklist/detail',
      method: "POST",
      data: {
        id:that.data.id,
        option: 2
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.code == 310) {
          wx.showModal({
            title: '提示',
            content: '图书不存在',
            showCancel:false
          })          
          wx.navigateBack()
        }else if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '获取信息失败',
            showCancel:false
          })
        }
        that.setData({
          bookInfo: res.data.data.bookInfo,
          bookStatus: res.data.data.bookStatus
        })
      }
    })
  },
  scanLibraryCode(){
    var that = this
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ["barCode"],
      success(res) {
        that.setData({
          libraryID : res.result
        })
      },
      fail(err) {
        console.error(err)
        wx.showToast({
          title: err.errMsg,
          icon: 'none'
        })
      }
    })
  },
  scanLibraryCodeCheck(){
    var that = this
    wx.scanCode({
      onlyFromCamera: true,
      scanType: ["barCode"],
      success(res) {
        that.setData({
          check_libraryID : res.result
        })
      },
      fail(err) {
        console.error(err)
        wx.showToast({
          title: err.errMsg,
          icon: 'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  showDialog(msg) {
    wx.hideLoading();
    wx.showModal({
      title: '提示',
      content: msg,
      showCancel:false
    })
  },
  bindSave: function(event) {
    var value = event.detail.value
    if (value.libraryID == "") {
      this.showDialog("图书馆编号不能为空")
    } else if (value.check_libraryID == "") {
      this.showDialog("图书馆编号未验证")
    } else if (value.libraryID != this.data.id) {
      this.showDialog("图书馆编号校验失败")
    } else {
      var that = this
      wx.request({
        url: 'https://'+CONFIG.domain+'/booklist/borrow',
        method: "POST",
        data: {
          token:App.globalData.token,
          libraryID: value.libraryID,
          option: that.data.bookStatus.status
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          if (res.data.code == 311) {
            that.showDialog("该图书已被借阅")
          } else if (res.data.code == 312) {
            that.showDialog("该图书已被归还")
          } else if (res.data.code == 313) {
            that.showDialog("您未借阅该图书")
          } else if (res.data.code != 200) {
            wx.showModal({
              title: '提示',
              content: '提交失败',
              showCancel:false
            })
          } else {
            if (that.data.bookStatus.status == 1) {
              that.showDialog("借阅成功")
            } else {
              that.showDialog("归还成功")
            }
            wx.redirectTo({
              url: '/pages/mine/book',
            })
          }
        }
      })
    }
  }
})