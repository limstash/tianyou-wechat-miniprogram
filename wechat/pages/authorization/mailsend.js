const App = getApp()
const CONFIG = require('../../config.js')
Page({
  mixins: [require('../../libs/weui/themeChanged')],
  data: {
    id: "",
    name: "",
    status: "",
    toast: false,
    loading: false,
    hideToast: false,
    hideLoading: false,
  },
  syncUserInfo: function (data) {
    if (data == null) {
      return
    }
    App.globalData.status = data.status;
    App.globalData.permission = data.permission;
    App.globalData.tempOrder = data.tempOrder;
  },
  resend: function(event) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })

    var that = this
    this.setData({
      loading: true
    });
    wx.request({
      url: 'https://'+CONFIG.domain+'/resend',
      method: "POST",
      data: {
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '重新发送邮件失败',
            showCancel:false
          })
          return;
        }
        that.setData({
            loading: false,
            hideLoading: false,
        });
        that.syncUserInfo(res.data.userinfo)
        that.setData({
          toast: true
        });
        setTimeout(() => {
            that.setData({
                hideToast: true
            });
            setTimeout(() => {
                that.setData({
                    toast: false,
                    hideToast: false,
                });
            }, 300);
        }, 3000);
      }
    })
  },
  onLoad: function(option) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })
    
    wx.showLoading({
      title: '加载中',
    })
    
    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/verifyinfo',
      method: "POST",
      data: {
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '拉取验证信息失败',
            showCancel:false
          })
          return;
        }
        that.syncUserInfo(res.data.userinfo)
        that.setData({
          id: res.data.data.ecjtuID,
          name: res.data.data.realname,
          status: res.data.data.status,
          permission: res.data.userinfo.permission
        })
      }
    })
  },
});