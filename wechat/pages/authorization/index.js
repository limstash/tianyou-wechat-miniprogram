const App = getApp()
const CONFIG = require('../../config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.backType = options.backType
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  navigateBack: function() {
    wx.switchTab({
      url: '../book/index'
    })
  },

  bindGetUserInfo: function(e) {
    let backtype = this.data.backType;
    if (e.detail.userInfo) {
      App.globalData.userInfo = e.detail.userInfo
      if (backtype =='index') {
        wx.switchTab({
          url: '../book/index'
        })
      } else if (backtype == 'mine') {
        wx.switchTab({
          url: '../mine/index'
        })
      } else if (backtype = 'identity') {
        wx.redirectTo({
          url: '../authorization/identity'
        })
      } else if (backtype = 'order') {
        wx.redirectTo({
          url: '../order/index'
        })
      }
    }
  }
})