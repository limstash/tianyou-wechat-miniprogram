const App = getApp()
const CONFIG = require('../../config.js')
Page({
  mixins: [require('../../libs/weui/themeChanged')],
  data : {
    id: "",
    name : "",
    loading: false,
    hideLoading: false
  },
  onLoad: function (options) {
    if (!App.globalData.userInfo) {
      wx.redirectTo({
        url: '../authorization/index?backType=identity'
      })
    }
  },
  idInput:function (e) {
    this.setData({
       id: e.detail.value
     })
  },
  nameInput:function (e) {
    this.setData({
       name: e.detail.value
     })
  }, 
  formSubmit: function (e) {
    wx.checkSession({
      fail: function(res){
        App.login()　　
      }
    })

    wx.showLoading({
      title: '加载中',
    })

    this.setData({
      loading: true
    });
    var that = this
    wx.request({
      url: 'https://'+CONFIG.domain+'/verify',
      method: "POST",
      data: {
        id: that.data.id,
        realname:that.data.name,
        nickname:App.globalData.userInfo.nickName,
        token:App.globalData.token
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        wx.hideLoading();
        if (res.data.code != 200) {
          wx.showModal({
            title: '提示',
            content: '信息不存在，验证失败',
            showCancel:false
          })
          return;
        }
        App.globalData.status = "1"
        wx.redirectTo({
          url: './mailsend'
        })
      }
    })
  }
});